// Create an instance of the Stripe object with your publishable API key
//var stripe = Stripe("pk_test_51Iqzq5IJacQPzCA1TRCL1TbvOWDh63wkOgPpxb9qovY43K3nipNKmNGY7YRSwwXB6jHFOQVplH61beRjCsFYdKH600GbIv35Ay");
var stripe = Stripe("pk_live_51Iqzq5IJacQPzCA1Ne303p1stjx61T90NjUqV8CUs4LfL30nM7liwp66u46QYZlGLgZFpBjTmpYXxuAdFsNvSQOr00xvB4vMAO");
$(function() {
    var $form = $('#payment-form');

    jQuery.extend(jQuery.validator.messages, {
        required: "Campo obligatorio.",
        remote: "Please fix this field.",
        email: "Formato incorrecto.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
    $form.validate({
        rules: {
            autonomo: { required: function () { 
                if ($("#empresa").is(":checked")) {
                    return false;
                } else {
                    return true;
                }
            }},
            empresa: { required: function () { 
                if ($("#autonomo").is(":checked")) {
                    return false;
                } else {
                    return true;
                }
            }},
            nif: { required: function () { return $("#autonomo").is(":checked"); } },
            razon: { required: function () { return $("#empresa").is(":checked"); } },
            cif: { required: function () { return $("#empresa").is(":checked"); } },
        }
    });  

    $form.submit(function(e){
        var isvalid = $form.valid();
        if (!isvalid) {
            return false;
        }
        e.preventDefault();
        e.stopPropagation();
        fetch("/create-checkout-session.php", {
            method: "POST",
            headers: {
                 'Content-Type': 'application/x-www-form-urlencoded',
              },
            body: $form.serialize()
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (session) {
            return stripe.redirectToCheckout({ sessionId: session.id });
        })
        .then(function (result) {
            if (result.error) {
                alert(result.error.message);
            }
        })
        .catch(function (error) {
            console.error("Error:", error);
        });
    });
});
