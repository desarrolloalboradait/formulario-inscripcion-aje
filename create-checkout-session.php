<?php

require 'vendor/autoload.php';
//\Stripe\Stripe::setApiKey('sk_test_51Iqzq5IJacQPzCA1xeyTqItunQznzvjXlIrH9ryZexvwNDb4n7XvENwfHMkFcZwNBouVH4sMxZSXyZNL6Fxpx8d600ZaaNIpDP');
\Stripe\Stripe::setApiKey('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');

header('Content-Type: application/json');

$domain = 'http://' . $_SERVER['HTTP_HOST'];

$postData = $_POST;

$checkout_session = \Stripe\Checkout\Session::create([
  'payment_method_types' => ['card'],
  'line_items' => [[
    'price_data' => [
      'currency' => 'eur',
      'unit_amount' => 9600,
      'product_data' => [
        'name' => 'Inscripción AJE Almería',
        'images' => ["https://www.ajeandalucia.org/wp-content/uploads/2016/02/logo-aje-andalucia-alta-transparente-e1454429883453.png"],
      ],
    ],
    'quantity' => 1,
  ]],
  'mode' => 'payment',
  'success_url' => $domain . '/success.php?' . http_build_query($postData),
  //'cancel_url' => $domain . '/cancel.html',
  'cancel_url' => $domain,
]);

echo json_encode(['id' => $checkout_session->id]);