<?php
  require 'vendor/autoload.php';
  require 'core.php';
  $data = $_GET;

  $title = "Nueva inscripción pagada";
  $nombre = !empty($data["nombre"]) ? $data["nombre"] : "";
  $autonomo = !empty($data["autonomo"]) ? "Si" : "No";
  $profesional = !empty($data["profesional"]) ? "Si" : "No";
  $email = !empty($data["email"]) ? $data["email"] : "";
  $direccion = !empty($data["direccion"]) ? $data["direccion"] : "";
  $localidad = !empty($data["localidad"]) ? $data["localidad"] : "";
  $cp = !empty($data["cp"]) ? $data["cp"] : "";
  $telefono = !empty($data["telefono"]) ? $data["telefono"] : "";
  $nif = !empty($data["nif"]) ? $data["nif"] : "";
  $razon = !empty($data["razon"]) ? $data["razon"] : "";
  $cif = !empty($data["cif"]) ? $data["cif"] : "";
  $observaciones = !empty($data["observaciones"]) ? $data["observaciones"] : "";
  $aceptar_recibir_comunicaciones = !empty($data["aceptar_recibir_comunicaciones"]) ? "Si" : "No";
  $aceptar_ceder_datos_terceros = !empty($data["aceptar_ceder_datos_terceros"]) ? "Si" : "No";

  $text = "=======================================\n";
  $text .= $title . "\n";
  $text .= "=======================================\n";
  $text .= "Nombre y apellidos: " . $nombre . "\n";
  $text .= "Autónomo: " . $autonomo . "\n";
  $text .= "Profesional: " . $profesional . "\n";
  $text .= "Email: " . $email . "\n";
  $text .= "Dirección: " . $direccion . "\n";
  $text .= "Localidad: " . $localidad . "\n";
  $text .= "C.P.: " . $cp . "\n";
  $text .= "Teléfono: " . $telefono . "\n";
  $text .= "NIF: " . $nif . "\n";
  $text .= "Razón social: " . $razon . "\n";
  $text .= "CIF: " . $cif . "\n";
  $text .= "Acepto recibir comunicaciones comerciales por parte de ASOCIACIÓN JÓVENES EMPRESARIOS DE ALMERÍA, sobre servicios y productos relacionados con los sectores de actividad reflejados en la Política de Privacidad: " . $aceptar_recibir_comunicaciones . "\n";
  $text .= "Acepto que ASOCIACIÓN JÓVENES EMPRESARIOS DE ALMERÍA ceda mis datos personales a terceras empresas relacionadas con los sectores de actividad que se detallan en la Política de Privacidad: " . $aceptar_ceder_datos_terceros . "\n";
  $text .= "Observaciones: " . $observaciones . "\n";
  $text .= "=======================================\n";

  $html = "<h1>" . $title . "</h1>";
  $html .= "<p><strong>Nombre y apellidos:</strong> " . $nombre . "</p>";
  $html .= "<p><strong>Autónomo:</strong> " . $autonomo . "</p>";
  $html .= "<p><strong>Profesional:</strong> " . $profesional . "</p>";
  $html .= "<p><strong>Email:</strong> " . $email . "</p>";
  $html .= "<p><strong>Dirección:</strong> " . $direccion . "</p>";
  $html .= "<p><strong>Localidad:</strong> " . $localidad . "</p>";
  $html .= "<p><strong>C.P.:</strong> " . $cp . "</p>";
  $html .= "<p><strong>Teléfono:</strong> " . $telefono . "</p>";
  $html .= "<p><strong>NIF:</strong> " . $nif . "</p>";
  $html .= "<p><strong>Razón social:</strong> " . $razon . "</p>";
  $html .= "<p><strong>CIF:</strong> " . $cif . "</p>";
  $html .= "<p><strong>Acepto recibir comunicaciones comerciales por parte de ASOCIACIÓN JÓVENES EMPRESARIOS DE ALMERÍA, sobre servicios y productos relacionados con los sectores de actividad reflejados en la Política de Privacidad:</strong> " . $aceptar_recibir_comunicaciones . "</p>";
  $html .= "<p><strong>Acepto que ASOCIACIÓN JÓVENES EMPRESARIOS DE ALMERÍA ceda mis datos personales a terceras empresas relacionadas con los sectores de actividad que se detallan en la Política de Privacidad:</strong> " . $aceptar_ceder_datos_terceros . "</p>";
  $html .= "<p><strong>Observaciones:</strong> " . $observaciones . "</p>";

  $email = new \SendGrid\Mail\Mail();
  $email->setFrom("noreply@ajeandalucia.org", "AJE Almería");
  $email->setSubject("Nueva inscripción pagada");
  //$email->addTo("juan@alboradait.com");
  $email->addTo("asociacionjovenesempresarios@gmail.com");
  $email->addContent("text/plain", $text);
  $email->addContent("text/html", $html);
  $sendgrid = new \SendGrid(SENGRID_API_KEY);
  try {
      $response = $sendgrid->send($email);
      /*
      print $response->statusCode() . "\n";
      print_r($response->headers());
      print $response->body() . "\n";
      */
  } catch (Exception $e) {
      //echo 'Caught exception: '. $e->getMessage() ."\n";
  }
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Inscripción AJE Almería</title>
    <meta name="viewport" content="width=device-width" , initial-scale="1.0" , maximum-scale="1.0" , user-scalable="1.0">
    <link rel="shortcut icon" href="img/android-icon-36x36.png">
    <link rel="apple-touch-icon" href="img/android-icon-144x144.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
</head>
<body>
<div class="container">
  <div class="row justify-content-md-center">
      <div class="col col-sm-12 text-center">
          <a href="https://www.ajeandalucia.org" target="_blank" title="AJE Almería"><img src="img/logo-aje.png" alt="AJE Almería" title="AJE Almería" /></a>
      </div>
      <div class="col col-sm-12 text-center" style="margin-top:15px;">
          <h1>Inscripción AJE Almería</h1>
      </div>
  </div>
  <div class="row">
    <div class="alert alert-success" role="alert" style="margin:50px auto;">
      Su inscripción se ha realizado correctamente. 
    </div>
  </div>
</div>
</body>
</html>
